variable "project_id" {
  description = "project id"
}

variable "region" {
  type = string
  description = "region"
}
variable "gcp_credentials" {
  type = string
  description = "gcp_credentials file"
}
